# Statistics in Biology

A handout/ quick-reference describing basic statistics that are commonly used in Biology.

To view the compiled document, download [statistics_in_biology.pdf](https://bitbucket.org/caseywdunn/statistics/src/master/statistics_in_biology.pdf).


